FROM docker.io/library/maven:3.8.6-eclipse-temurin-17-focal AS builder

ARG MAVEN_OPTS="-Dhttps.protocols=TLSv1.2 -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
ARG MAVEN_CLI_OPTS="--batch-mode --errors --fail-at-end --show-version"

WORKDIR /src
COPY . .
RUN mvn $MAVEN_CLI_OPTS -DskipTests package

FROM docker.io/library/eclipse-temurin:17.0.5_8-jre-alpine

# TODO Run the app as a non-root user
WORKDIR /opt/sitodo
COPY --from=builder /src/target/*.jar sitodo.jar

CMD ["java", "-jar", "sitodo.jar"]
